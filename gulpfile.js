'use strict';

let MODE = 'dev';

const path = require('path');
const gulp = require('gulp');
const del = require('del');
const prefixer = require('gulp-autoprefixer');
const sass = require('gulp-sass');
const cssmin = require('gulp-clean-css');
const base64 = require('gulp-base64-inline');
const sassFuncs = require('node-sass-asset-functions');
const plumber = require('gulp-plumber');
const includefile = require('gulp-rigger');
const rename = require('gulp-rename');
const sourcemaps = require('gulp-sourcemaps');
const browserSync = require('browser-sync').create();
const cabinet = require('filing-cabinet');
const resolve = require('resolve');

const paths = {
	build: {
		css: 'css/',
		html: 'pages/',
	},
	src: {
		styles: 'sass/main.scss',
		html: 'pages/src/*.html',
	},
	watch: {
		styles: 'sass/**/*.scss',
		html: 'pages/src/**/*.html'
	},
	clean: [
		'css/*',
		'pages/*.html',
	],
	vendor: {
	}
};


function build_Markup () {
	return gulp.src(paths.src.html)
		.pipe(includefile())
		.pipe(gulp.dest(paths.build.html));
}

function build_Styles () {
	var tmp = gulp.src(paths.src.styles)
		.pipe(rename({basename: 'styles'}));
	if (MODE === 'prod') {
		tmp = tmp
			.pipe(plumber())
			.pipe(sass({
				outputStyle: 'compressed',
				functions: sassFuncs({
					images_path: './img',
					fonts_path: './fonts',
					http_images_path: '../img',
					http_fonts_path: '../fonts',
				})
			}))
			.pipe(base64())
			.pipe(cssmin({ inline: ['local'], level: 0 }))
			.pipe(prefixer("last 2 version"))
			.pipe(gulp.dest(paths.build.css))
			.pipe(cssmin({ inline: ['local'], level: 2 }))
			.pipe(plumber.stop())
			.pipe(rename({extname: '.min.css'}))
			.pipe(gulp.dest(paths.build.css));
	} else {
		tmp = tmp
			.pipe(plumber())
			.pipe(sourcemaps.init())
			.pipe(sass({
					outputStyle: 'expanded',
					functions: sassFuncs({
						images_path: './img',
						fonts_path: './fonts',
						http_images_path: '../img',
						http_fonts_path: '../fonts',
					})
				}).on('error', function (err) {
					console.log(err.toString());
					browserSync.notify(err.message, 5000);
				})
			)
			.pipe(base64())
			.pipe(cssmin({ inline: ['local'], level: 0 }))
			.pipe(prefixer("last 2 version"))
			.pipe(gulp.dest(paths.build.css))
			.pipe(plumber.stop());
	}

	return tmp.pipe(browserSync.stream());
}

function resolveDepFile(file, dep, ast = null) {
	if (dep === 'module' || dep === 'exports') return null;

	let depFile = cabinet({
		partial: dep,
		filename: file,
		directory: path.join(__dirname, paths.build.js),
		ast: ast,
		config: { paths: paths.vendor.js },
		nodeModulesConfig: { entry: 'module' },
	});
	if (!depFile) {
		try {
			depFile = resolve.sync(dep);
		} catch (e) {}
	}
	return depFile || null;
}

function resolveDep(file, dep, ast = null) {
	let depFile = resolveDepFile(file, dep, ast);
	if (depFile && depFile.indexOf('node_modules') > -1) {
		return dep;
	}
	return depFile || null;
}

function watch (done) {
	gulp.watch([paths.watch.styles], build_Styles);
	gulp.watch([paths.watch.html], gulp.series(build_Markup, reload));
	done();
}

function clean (done) {
	return del(paths.clean);
}

function runServer (done) {
	browserSync.init({
		server: {
			baseDir: ['./', './pages'],
			logLevel: 'debug',
			middleware: [

			]
		},
		ghostMode: false,
	});
	done();
}

function reload (done) {
	browserSync.reload();
	done();
}

function setDevMode (done) {
	MODE = 'dev';
	done();
}

const build = gulp.series(
	setDevMode,
	clean,
	gulp.parallel(
		gulp.series(
			build_Markup
		),
		build_Styles
	)
);

const serve = gulp.series(runServer, watch);
const defaultTask = gulp.series(build, serve);


exports.build = build;
exports.clean = clean;
exports.serve = serve;
exports.default = defaultTask;
